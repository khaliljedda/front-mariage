
import Vuex from "vuex";
import inscription from "./inscription";
import Vue from "vue";
Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        datat:[],
    },
    getters: {
        datat: state =>state.datat
    },

    mutations: {},

    actions: {},

    modules: {
    inscription
    }
});